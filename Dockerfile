FROM ubuntu:22.04 as base
RUN apt-get update  && \
    apt-get install nano python3 python3-pip python3-yaml -y && \
    rm -rf /var/lib/apt/lists/*
RUN mkdir /opt/ts3proxy
WORKDIR /opt/ts3proxy
COPY . .
RUN bash setup.sh
ENTRYPOINT ["python3","-m","ts3proxy"]